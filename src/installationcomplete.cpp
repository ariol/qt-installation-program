#include "installationcomplete.h"
#include "ui_installationcomplete.h"
#include "main.h"

InstallationComplete::InstallationComplete(QString installPath, bool isInstallSucceed, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InstallationComplete),
    installPath(installPath),
    isInstallSucceed(isInstallSucceed)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("%1 %2 安装").arg(xin::exeName).arg(xin::versions));
    ui->labelWallpaper->setScaledContents(true);
    ui->labelWallpaper->setPixmap(QPixmap(":/resourceFile/123.png"));
    ui->labelTitleComplete->setWordWrap(true);
    ui->labelTitleComplete->setFont(QFont("Times", 12, QFont::Bold));
    if (isInstallSucceed) {
        ui->labelTitleComplete->setText(tr("%1 %2 已经完成安装").arg(xin::exeName).arg(xin::versions));
        ui->textEditcontent->setText(tr("%1 %2 已安装在您的系统. \n\n单击 [完成] 关闭此向导").arg(xin::exeName).arg(xin::versions));

        ui->pushButtonCancel->setText(tr("取消(C)"));
        ui->pushButtonCancel->setShortcut(Qt::Key_C);
        ui->pushButtonfinish->setText(tr("完成(F)"));
        ui->pushButtonCancel->setShortcut(Qt::Key_F);
        ui->pushButtonLastStep->setText(tr("< 上一步(B)"));
        ui->pushButtonCancel->setShortcut(Qt::Key_B);
        ui->pushButtonLastStep->setEnabled(false);
#if defined (Q_OS_LINUX)
        ui->checkBoxIsDesktop->hide();
        ui->checkBox->hide();
#else
        ui->checkBox->setChecked(true);
        ui->checkBoxIsDesktop->setChecked(true);
        ui->checkBoxIsDesktop->setText(tr("创建桌面快捷方式"));
        ui->checkBox->setText(tr("运行 ") + xin::exeName + xin::versions);
#endif
    }
    else {
        ui->labelTitleComplete->setText(tr("%1 %2 安装失败").arg(xin::exeName).arg(xin::versions));
        ui->textEditcontent->setText(tr("%1 %2 安装失败,请检查后重新安装. \n\n单击 [重新安装] 继续此向导").arg(xin::exeName).arg(xin::versions));

        ui->pushButtonCancel->setText(tr("取消(C)"));
        ui->pushButtonCancel->setShortcut(Qt::Key_C);
        ui->pushButtonfinish->setText(tr("重新安装(A)"));
        ui->pushButtonfinish->setShortcut(Qt::Key_A);
        ui->pushButtonLastStep->setText(tr("< 上一步(B)"));
        ui->pushButtonLastStep->setShortcut(Qt::Key_B);
        ui->pushButtonLastStep->setEnabled(false);

        ui->checkBoxIsDesktop->hide();
        ui->checkBox->hide();
    }

    ui->textEditcontent->setFrameShape(QFrame::Shape::NoFrame); //设置无边框
    ui->textEditcontent->setReadOnly(true);
    ui->textEditcontent->setCursor(QCursor(Qt::CursorShape::ArrowCursor)); //如果不显示该鼠标!一定属性里面在点一下!

    connect(ui->pushButtonCancel, SIGNAL(clicked(bool)), this, SLOT(buttonCancel()));
    connect(ui->pushButtonfinish, SIGNAL(clicked(bool)), this, SLOT(buttonFinish()));
}

InstallationComplete::~InstallationComplete()
{
    delete ui;
}

void InstallationComplete::buttonFinish()
{
    //需修改
    //Linux此处需要修改，裸机需要配置环境变量或者拷贝动态库到共享库，所以直接启动可能启动不了。
    if (isInstallSucceed) {
        if (ui->checkBox->checkState()) {
            //需自行修改启动进程名称
            QString str = QString("\"%1%2\"").arg(installPath).arg(xin::executableExeName);
            QProcess *p = new QProcess(this);
            p->start(str);
        }
        if (ui->checkBoxIsDesktop->checkState()) {
            addLinkToDeskTop(installPath + xin::executableExeName);
        }
        exit(1);
    }
    else {
        //获取当前程序路径,重新启动!!
        QProcess* p = new QProcess();
        qDebug() << QCoreApplication::applicationFilePath();
        p->start(QCoreApplication::applicationFilePath(), QStringList() << "666666"); //带参跳过二次启动
        exit(0);
    }
}

void InstallationComplete::buttonCancel()
{
    exit(1);
}

void InstallationComplete::addLinkToDeskTop(const QString &filename)
{
    //Linux
#if defined (Q_OS_WINDOWS)
    //建立桌面快捷方式
    QString strAppPath = filename;
    QString strDesktopLink = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/";
    strDesktopLink += xin::exeName + ".lnk";
    QFile fApp(strAppPath);
    fApp.link(strDesktopLink);

    //建立开始菜单快捷方式
    QString strMenuLink = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation) + "/";
    strMenuLink += "notepad/";
    QDir pathDir;
    pathDir.mkpath(strMenuLink);
    strMenuLink += xin::exeName + ".lnk";
    fApp.link(strMenuLink);
#else
    Q_UNUSED(filename);
#endif
}
